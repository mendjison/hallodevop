package de.pivota.hallodevop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rest/")
public class HelloWord {

	@GetMapping("hello")
	private String sayHello() {
		return "Hello World";
	}
	
	@GetMapping("test")
	String home() {
		return "Spring is here!";
	}
}
