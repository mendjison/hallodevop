package de.pivota.hallodevop;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class HalloDevopApplication {

	public static void main(String[] args) {
		SpringApplication.run(HalloDevopApplication.class, args);
	}
}